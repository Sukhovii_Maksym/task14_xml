package service;

import domain.Gem;

import java.util.Comparator;

public class CompareByName implements Comparator<Gem> {
    public int compare(Gem o1, Gem o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
