package service;

import domain.Gem;

import java.util.Comparator;

public class SortByNameAndValue implements Comparator<Gem> {


    public int compare(Gem o1, Gem o2) {
        int byName = o1.getName().compareTo(o2.getName());
        int byValue = o1.getValue() >= o2.getValue()?1:-1;

        if (byName ==0){
            return byValue;
        }else
            return  byName;
    }
}
