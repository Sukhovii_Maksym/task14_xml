package parsers;

import domain.Gem;
import domain.Preciousness;
import domain.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class ParserSAX extends DefaultHandler {
    private Gem gem = new Gem();
    private VisualParameters gemVP = new VisualParameters();
    private String thisElement = "";


    @Override
    public void startDocument() {
        System.out.println("Start parse XML with SAX...");
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
        thisElement = qName;
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) {
        if (qName.equals("gem")) {
            System.out.println(gem);
        }
        thisElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (thisElement.equals("name")) {
            gem.setName(new String(ch, start, length));
        }
        if (thisElement.equals("preciousness")) {
            String prec = new String(ch, start, length);
            if (prec.equals("precious")) {
                gem.setPreciousness(Preciousness.precious);
            } else gem.setPreciousness(Preciousness.semiPrecious);
        }

        if (thisElement.equals("origin")) {
            gem.setOrigin(new String(ch, start, length));
        }
        if (thisElement.equals("color")) {
            gemVP.setColor(new String(ch, start, length));
        }
        if (thisElement.equals("transparency")) {
            gemVP.setTransparency(new Double(new String(ch, start, length)));
        }
        if (thisElement.equals("faces")) {
            Integer numberOfFaces = new Integer(new String(ch, start, length));
            gemVP.setNumberOfFaces(numberOfFaces);
            gem.setVisualParameters(gemVP);

        }
        if (thisElement.equals("value")) {
            gem.setValue(new Double(new String(ch, start, length)));
        }


    }

    @Override
    public void endDocument() {
        System.out.println("Stop parse XML with SAX...");
    }

    public void start() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        ParserSAX saxp = new ParserSAX();
        parser.parse(new File("src/main/resources/xml/Catalog.xml"), saxp);
    }

}