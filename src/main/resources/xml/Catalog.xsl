<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns="http://www.w3.org/1999/xhtml">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head> <title>Gems!!!</title>
            <style>
                td{
                text-align:center;
                }
            </style>
            </head>
            <body>
                <h2 align="center">Gems Collection</h2>
                <table border="1" width = "60%" align="center">
                    <tr bgcolor="#9acd32">
                        <th rowspan="2">Name</th>
                        <th rowspan="2">Preciousness</th>
                        <th rowspan="2">Origin</th>
                        <th colspan="3">Visual Parameters</th>
                        <th rowspan="2">Value</th>
                    </tr>
                    <tr bgcolor="orange">
                        <th >Color</th>
                        <th>Transparency</th>
                        <th>Faces</th>
                    </tr>
                    <xsl:for-each select="gems/gem">
                        <tr>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="preciousness"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/color"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/transparency"/>
                            </td>
                            <td>
                                <xsl:value-of select="visualParameters/faces"/>
                            </td>
                            <td>
                                <xsl:value-of select="value"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>